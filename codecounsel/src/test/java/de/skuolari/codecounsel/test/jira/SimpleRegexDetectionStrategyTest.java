package de.skuolari.codecounsel.test.jira;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.skuolari.codecounsel.jira.SimpleRegexDetectionStrategy;

public class SimpleRegexDetectionStrategyTest {

	
	@Test
	public void testRegexDetectionStrategy() {
		SimpleRegexDetectionStrategy strategy = new SimpleRegexDetectionStrategy();
		assertEquals("FOO-123", strategy.detectIssue("FOO-123 Add new mapper to bluebox"));
	}
}
