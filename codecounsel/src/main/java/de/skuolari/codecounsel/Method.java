package de.skuolari.codecounsel;

public interface Method {

	public String getSimpleName();
	public String getFQN();
}
