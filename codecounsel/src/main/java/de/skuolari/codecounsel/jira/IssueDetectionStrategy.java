package de.skuolari.codecounsel.jira;

public interface IssueDetectionStrategy {


	public String detectIssue(String s);
}
