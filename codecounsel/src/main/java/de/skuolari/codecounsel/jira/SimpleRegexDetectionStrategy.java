package de.skuolari.codecounsel.jira;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleRegexDetectionStrategy implements IssueDetectionStrategy {

	Pattern issueIDPattern = Pattern.compile("\\p{Alpha}+-[0-9]+");
	
	public String detectIssue(String s) {
		Matcher m = issueIDPattern.matcher(s);
		if(m.find()) {
			return m.group();
		}
		return null;
	
	}

}
